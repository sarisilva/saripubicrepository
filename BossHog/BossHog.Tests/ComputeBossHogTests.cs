﻿namespace BossHog.Tests
{
    using BossHog.Shared;

    using NUnit.Framework;

    [TestFixture]
    public class ComputeBossHogTests
    {
        #region Public Methods

        [Test]
        public void MultiplesOfThreeTest()
        {
            string expectedOutput = "1 2  Boss 4 5  Boss 7 8  Boss 10 11  Boss 13 14  Boss 16 17  Boss 19 20  Boss 22 23  Boss 25 26  Boss 28 29  Boss 31 32  Boss 34 35  Boss 37 38  Boss 40 41  Boss 43 44  Boss 46 47  Boss 49 50  Boss 52 53  Boss 55 56  Boss 58 59  Boss 61 62  Boss 64 65  Boss 67 68  Boss 70 71  Boss 73 74  Boss 76 77  Boss 79 80  Boss 82 83  Boss 85 86  Boss 88 89  Boss 91 92  Boss 94 95  Boss 97 98  Boss 100 ";
            ComputeBossHog computeBossHog = new ComputeBossHog();
            string output = computeBossHog.GetOutput(Enums.Multiples.Three);

            Assert.AreEqual(output, expectedOutput);
        }

        [Test]
        public void MultiplesOfFiveTest()
        {
            string expectedOutput = "1 2 3 4  Hog 6 7 8 9  Hog 11 12 13 14  Hog 16 17 18 19  Hog 21 22 23 24  Hog 26 27 28 29  Hog 31 32 33 34  Hog 36 37 38 39  Hog 41 42 43 44  Hog 46 47 48 49  Hog 51 52 53 54  Hog 56 57 58 59  Hog 61 62 63 64  Hog 66 67 68 69  Hog 71 72 73 74  Hog 76 77 78 79  Hog 81 82 83 84  Hog 86 87 88 89  Hog 91 92 93 94  Hog 96 97 98 99  Hog ";
            ComputeBossHog computeBossHog = new ComputeBossHog();
            string output = computeBossHog.GetOutput(Enums.Multiples.Five);

            Assert.AreEqual(output, expectedOutput);
        }

        [Test]
        public void MultiplesOfThreeAndFiveTest()
        {
            string expectedOutput = "1 2 3 4 5 6 7 8 9 10 11 12 13 14  BossHog 16 17 18 19 20 21 22 23 24 25 26 27 28 29  BossHog 31 32 33 34 35 36 37 38 39 40 41 42 43 44  BossHog 46 47 48 49 50 51 52 53 54 55 56 57 58 59  BossHog 61 62 63 64 65 66 67 68 69 70 71 72 73 74  BossHog 76 77 78 79 80 81 82 83 84 85 86 87 88 89  BossHog 91 92 93 94 95 96 97 98 99 100 ";
            ComputeBossHog computeBossHog = new ComputeBossHog();
            string output = computeBossHog.GetOutput(Enums.Multiples.ThreeAndFive);

            Assert.AreEqual(output, expectedOutput);
        }

        #endregion
    }
}