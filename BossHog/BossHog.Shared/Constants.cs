﻿namespace BossHog.Shared
{
    public class Constants
    {
        #region Static Fields and Constants

        public const int Start = 1;

        public const int End = 100;

        public const string Seperator = " ";

        #endregion
    }
}