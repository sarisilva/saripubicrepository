﻿namespace BossHog.Shared
{
    public class Enums
    {
        #region Public Enums

        public enum Multiples
        {
            Three = 1,

            Five = 2,

            ThreeAndFive = 3
        }

        #endregion
    }
}