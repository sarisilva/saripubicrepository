﻿namespace BossHog
{
    using BossHog.BAL;
    using BossHog.Shared;

    public class ComputeBossHog
    {
        #region Instance Fields

        private readonly IPrintNumbers iPrintNumbers = new PrintNumbers();

        #endregion

        #region Public Methods

        public string GetOutput(Enums.Multiples multipleType)
        {
            return this.iPrintNumbers.Print(multipleType);
        }

        #endregion
    }
}