﻿namespace BossHog.BAL
{
    using System;
    using System.Text;

    using BossHog.Shared;


    /// <summary>
    /// This class prints "BossHog" at the multiples of 3 and 5
    /// </summary>
    public class MultiplesOfThreeAndFive : IMultiple
    {
        private const string stringToPrint = "BossHog";

        public string Compute()
        {
            StringBuilder stringBuilder = new StringBuilder();

            for (int i = Constants.Start; i <= Constants.End; i++)
            {
                if (i % 3 == 0 && i % 5 == 0)
                {
                    // multiples of 3 and 5
                    stringBuilder.Append(Constants.Seperator);
                    stringBuilder.Append(stringToPrint);
                    stringBuilder.Append(Constants.Seperator);
                }
                else
                {
                    stringBuilder.Append(i);
                    stringBuilder.Append(Constants.Seperator);
                }
            }
            return stringBuilder.ToString();
        }
    }
}
