﻿namespace BossHog.BAL
{
    using System;
    using System.Text;

    using BossHog.Shared;


    /// <summary>
    /// This class prints "Hog" at the multiples of 5
    /// </summary>
    public class MultiplesOfFive : IMultiple
    {
        private const string stringToPrint = "Hog";

        public string Compute()
        {
            StringBuilder stringBuilder = new StringBuilder();

            for (int i = Constants.Start; i <= Constants.End; i++)
            {
                if (i % 5 == 0)
                {
                    // multiple of 5
                    stringBuilder.Append(Constants.Seperator);
                    stringBuilder.Append(stringToPrint);
                    stringBuilder.Append(Constants.Seperator);
                }
                else
                {
                    stringBuilder.Append(i);
                    stringBuilder.Append(Constants.Seperator);
                }
            }
            return stringBuilder.ToString();
        }
    }
}
