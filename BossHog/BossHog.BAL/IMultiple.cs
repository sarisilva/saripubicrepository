﻿namespace BossHog.BAL
{
    public interface IMultiple
    {
        #region Public Methods

        string Compute();

        #endregion
    }
}