﻿namespace BossHog.BAL
{
    using BossHog.Shared;

    public interface IPrintNumbers
    {
        #region Public Methods

        string Print(Enums.Multiples multipleType);

        #endregion
    }
}