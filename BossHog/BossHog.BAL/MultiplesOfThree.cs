﻿namespace BossHog.BAL
{
    using System.Text;
    using BossHog.Shared;

    /// <summary>
    /// This class prints "Boss" at the multiples of 3
    /// </summary>
    public class MultiplesOfThree : IMultiple
    {
        private const string stringToPrint = "Boss";

        public string Compute()
        {
            StringBuilder stringBuilder = new StringBuilder();

            for (int i = Constants.Start; i <= Constants.End; i++)
            {
                if (i % 3 == 0)
                {
                    // multiple of 3
                    stringBuilder.Append(Constants.Seperator);
                    stringBuilder.Append(stringToPrint);
                    stringBuilder.Append(Constants.Seperator);
                }
                else
                {
                    stringBuilder.Append(i);
                    stringBuilder.Append(Constants.Seperator);
                }
            }
            return stringBuilder.ToString();
        }
    }
}
