﻿namespace BossHog.BAL
{
    using System;

    using BossHog.Shared;

    public class MultipleFactory
    {
        #region Public Methods

        public IMultiple GetType(Enums.Multiples multipleType)
        {
            switch (multipleType)
            {
                case Enums.Multiples.Three:
                    return new MultiplesOfThree();

                case Enums.Multiples.Five:
                    return new MultiplesOfFive();

                case Enums.Multiples.ThreeAndFive:
                    return new MultiplesOfThreeAndFive();

                default: throw new InvalidOperationException("This type is not supported");
            }

        }

        #endregion
    }
}