﻿namespace BossHog.BAL
{
    using BossHog.Shared;

    public class PrintNumbers : IPrintNumbers
    {
        #region Public Methods

        public string Print(Enums.Multiples multipleType)
        {
            MultipleFactory multipleFactory = new MultipleFactory();

            return multipleFactory.GetType(multipleType).Compute();
        }

        #endregion
    }
}