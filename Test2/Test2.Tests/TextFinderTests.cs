﻿namespace Test2.Tests
{
    using NUnit.Framework;

    [TestFixture]
    public class TextFinderTests
    {
        #region Static Fields and Constants

        private const string textToSearch = "Peter told me (actually he slurrred) that peter the pickle piper piped a pitted pickle before he petered out. Phew!";

        #endregion

        #region Instance Fields

        private readonly TextFinder textFinder = new TextFinder();

        #endregion

        #region Public Methods

        [Test]
        public void GetSubText_GetsSubTextSuccessfully_Test1()
        {
            string subText = "Peter";
            Assert.AreEqual("1, 43, 98", this.textFinder.GetSubText(textToSearch, subText));
        }

        [Test]
        public void GetSubText_GetsSubTextSuccessfully_Test2()
        {
            string subText = "peter";
            Assert.AreEqual("1, 43, 98", this.textFinder.GetSubText(textToSearch, subText));
        }

        [Test]
        public void GetSubText_GetsSubTextSuccessfully_Test3()
        {
            string subText = "Pick";
            Assert.AreEqual("53, 81", this.textFinder.GetSubText(textToSearch, subText));
        }

        [Test]
        public void GetSubText_GetsSubTextSuccessfully_Test4()
        {
            string subText = "Pi";
            Assert.AreEqual("53, 60, 66, 74, 81", this.textFinder.GetSubText(textToSearch, subText));
        }

        [Test]
        public void GetSubText_GetsSubTextSuccessfully_Test5()
        {
            string subText = "Z";
            Assert.AreEqual(string.Empty, this.textFinder.GetSubText(textToSearch, subText));
        }

        #endregion
    }
}