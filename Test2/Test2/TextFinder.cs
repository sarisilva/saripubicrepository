﻿namespace Test2
{
    using Test2.BAL;

    public class TextFinder
    {
        #region Instance Fields

        private readonly ISearch iSearch;

        #endregion

        #region Constructors

        public TextFinder()
        {
            this.iSearch = new SearchText();
        }

        #endregion

        #region Public Methods

        public string GetSubText(string textToSearch, string subText)
        {
            return this.iSearch.Find(textToSearch, subText);
        }

        #endregion
    }
}