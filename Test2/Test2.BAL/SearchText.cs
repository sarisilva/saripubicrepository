﻿namespace Test2.BAL
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// This class finds all the occurrences of a particular set of characters in a string.
    /// No string finding methods of IndexOf, substring, regular expression classes were used in this class.
    /// </summary>
    public class SearchText : ISearch
    {
        #region Instance Fields

        private string subText;

        private string textToSearch;

        #endregion

        #region Properties and Indexers

        public string TextToSearch
        {
            get
            {
                return this.textToSearch;
            }
            private set
            {
                this.textToSearch = !string.IsNullOrEmpty(value) ? value.ToLower() : string.Empty;
            }
        }

        public string SubText
        {
            get
            {
                return this.subText;
            }
            private set
            {
                this.subText = !string.IsNullOrEmpty(value) ? value.ToLower() : string.Empty;
            }
        }

        #endregion

        #region ISearch Members

        public string Find(string stringToSearch, string subString)
        {
            this.TextToSearch = stringToSearch;

            this.SubText = subString;

            List<string> indexesToReturn = new List<string>();

            List<char> foundChars = new List<char>();

            List<int> index = new List<int>();

            for (int textToSearchIndex = 0; textToSearchIndex < this.textToSearch.Length; textToSearchIndex++)
            {
                bool charFound = false;

                for (int subTextIndex = 0; subTextIndex < this.subText.Length; subTextIndex++)
                {
                    // subtext character found in the textTosearch, so continue to next character in textToSearch
                    if (charFound) break;

                    // always refer to the subsequent character in subText
                    if (foundChars.Count != subTextIndex) continue;

                    if (this.textToSearch[textToSearchIndex].Equals(this.subText[subTextIndex]))
                    {
                        charFound = true;

                        index.Add(textToSearchIndex + 1);

                        foundChars.Add(this.subText[subTextIndex]);
                    }
                    else
                    {
                        // no match, hence clear the counters.
                        foundChars.Clear();

                        index.Clear();
                    }
                }

                if (foundChars.Count != this.subText.Length) continue;

                indexesToReturn.Add(index.FirstOrDefault().ToString());

                foundChars.Clear();

                index.Clear();
            }

            string result = string.Join(", ", indexesToReturn);

            return result;
        }

        #endregion
    }
}