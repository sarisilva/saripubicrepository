﻿namespace Test2.BAL
{
    public interface ISearch
    {
        #region Public Methods

        string Find(string textToSearch, string subText);

        #endregion
    }
}